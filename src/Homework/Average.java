package Homework;

public class Average {
	private String name;
	private double sum;
	private int number;
	
	public Average(String n, double s, int a){
		name = n;
		sum = s;
		number = a;
	}
	
	public String getAverage(){
		return "\t " + name + "\t\t      " + sum/number;
	}
	
}
