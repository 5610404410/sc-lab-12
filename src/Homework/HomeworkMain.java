package Homework;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;

import PhoneBook.PhoneBook;

public class HomeworkMain {

	public static void main(String[] args) {
		writeHomework();
		writeAverage();
	}
	public static void writeHomework(){
		FileWriter fileWriter = null;
	 	 try {
			 // read from user
			 InputStreamReader inReader = new InputStreamReader(System.in);
			 BufferedReader buffer = new BufferedReader(inReader);
			 // write to file
			 fileWriter = new FileWriter("homework.txt");
			 PrintWriter out = new PrintWriter(fileWriter);
			 System.out.println("Input text to file:");
			 String line = buffer.readLine();
			 while (!line.equals("bye")) {	
				 
				 Homework hw = new Homework(line);
				 String v = hw.getValue();
				 out.println(v);
				 line = buffer.readLine();
				 
			 }		 	
			 out.flush();
	 	 }
	 	 catch (IOException e){
			 System.err.println("Error reading from user");
	 	 }
	}
	
	public static void writeAverage(){
		 FileWriter fileWriter = null;
		  try {
		      FileReader fileReader = new FileReader("homework.txt");
		      BufferedReader buffer = new BufferedReader(fileReader);
		    
		    
		      fileWriter = new FileWriter("average.txt");
		      PrintWriter out = new PrintWriter(fileWriter);
		      
		      out.println("Open file:");
		      out.println("\t>>>> Homework Score <<<<\n");
		      out.println("\t Name \t\t Average score");
		      String line = buffer.readLine();
		      while (line != null) {
		       String[] message = line.split(",");
		       String name = message[0].trim();
		       int amount = (message.length-1);
		       double sum = 0;
		       for(int i=1;i<message.length;i++){
		        sum += Double.parseDouble(message[i]);
		       }
		       
		       Average av = new Average(name, sum, amount);
		       out.println(av.getAverage());   
		       line = buffer.readLine();
		      }    
		      out.flush();
		       }
		     		       
		       catch (IOException e){
		       System.err.println("Error reading from file");
		       }
	}

}
