package PhoneBook;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class PhoneBookMain {

	public static void main(String[] args) {
		showPhonebook();
	}
	
	public static void showPhonebook(){
		try {
			FileReader fileReader = new FileReader("phonebook.txt");
			BufferedReader buffer = new BufferedReader(fileReader);
			
			System.out.println("Open file:");
			System.out.println("\t>>>>>> Java Phone Book <<<<<<\n");
			System.out.println("\t Name \t\t PhoneNumber");
			String line = buffer.readLine();
						 
			while (line != null) {
				String[] message = line.split(",");
				String name = message[0].trim();
				String phone = message[1].trim();
				
				PhoneBook pb = new PhoneBook(name, phone);
				System.out.println(pb.getJavaPhoneBook());				
				line = buffer.readLine();
			}
		}
		catch (IOException e){
			System.err.println("Error reading from file");
		}
	}
}
