package PhoneBook;

public class PhoneBook {
	private String nameJava;
	private String phoneJava;
	
	public PhoneBook(String n, String p){
		nameJava = n;
		phoneJava = p;
	}
	
	public String getJavaPhoneBook(){
		return "\t " + nameJava + "\t\t " + phoneJava;
	}
}
