package ExamScore;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;

import Homework.Average;
import Homework.Homework;

public class ExamMain {

	public static void main(String[] args) {
		writeExamScore();
		writeAverage();
	}
	
	public static void writeExamScore(){
		FileWriter fileWriter = null;
	 	 try {
			 // read from user
			 InputStreamReader inReader = new InputStreamReader(System.in);
			 BufferedReader buffer = new BufferedReader(inReader);
			 // write to file
			 fileWriter = new FileWriter("exam.txt");
			 PrintWriter out = new PrintWriter(fileWriter);
			 System.out.println("Input text to file:");
			 String line = buffer.readLine();
			 while (!line.equals("bye")) {					 
				 Exam ex = new Exam(line);
				 String s = ex.getScore();
				 out.println(s);
				 line = buffer.readLine();
				 
			 }		 	
			 out.flush();
	 	 }
	 	 catch (IOException e){
			 System.err.println("Error reading from user");
	 	 }
	}
	 public static void writeAverage(){
		 FileWriter fileWriter = null;
		  try {
		      FileReader fileReader = new FileReader("exam.txt");
		      BufferedReader buffer = new BufferedReader(fileReader);
		    
		    
		      fileWriter = new FileWriter("average.txt",true);
		      PrintWriter out = new PrintWriter(fileWriter);
		      
//		      out.println("Open file:");
		      out.println("\n\t>>>> Average Exam Score <<<<\n");
		      out.println("\t Name \t\t Average score");
		      String line = buffer.readLine();
		      while (line != null) {
		       String[] message = line.split(",");
		       String name = message[0].trim();
		       int amount = (message.length-1);
		       double sum = 0;
		       for(int i=1;i<message.length;i++){
		        sum += Double.parseDouble(message[i]);
		       }
		       
		       Average av = new Average(name, sum, amount);
		       out.println(av.getAverage());   
		       line = buffer.readLine();
		      }    
		      out.flush();
		       }
		     		       
		       catch (IOException e){
		       System.err.println("Error reading from file");
		       }
	 }
}
